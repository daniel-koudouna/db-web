class UserController < ApplicationController
 
  get '/login' do
    haml :login
  end

  post '/login' do
    res = client_safe_query "select * from customer where first_name = ? and email = ?", params[:name], params[:email]
    @user = res.first
    if (@user != nil )
      puts "USER = #{@user.inspect}"

      session[:user] = @user[:customer_id]

      puts session[:user]
      redirect "/cs3101/orders"
    else
      redirect "/cs3101/login", 303
    end
  end

  get '/register' do
    haml :register
  end
  
  post '/register' do
    params[:first_name] ||= ""
    params[:last_name] ||= ""
    params[:email] ||= ""
    params[:addr_street] ||= "Jack Cole Building, North Haugh"
    params[:addr_city] ||= "St Andrews"
    params[:addr_postcode] ||= "KY16 9SX"
    params[:preference_id] ||= "none"

    if (params[:first_name].length < 3 ||
        params[:last_name].length < 3 ||
        params[:email].length < 5)
      flash[:warn] = "Your data was invalid"
      redirect back
    end
    
    begin
      client_safe_query "insert into customer 
(first_name, last_name, addr_city, addr_street, addr_postcode, email, preference_id)
values (?, ?, ?, ? , ?, ? ,?)",
        params[:first_name], params[:last_name], params[:addr_city], params[:addr_street], params[:addr_postcode], params[:email], params[:preference_id]

      flash[:success] = "Registration successful!"
      redirect "/cs3101/login"
    rescue Mysql::Error => e
      puts e.error
      flash[:warn] = "Something went wrong! :("
      redirect "/cs3101/register", 303
    end
 end
  
  get '/logout' do
    session[:user] = nil
    redirect back
  end    
  
  get '/orders', logged_in: true do
    @orders = client_safe_query "select * from orders natural join restaurant where customer_id=?" , session[:user]
    puts "ORDERS = #{@orders.inspect}"
    haml :orders
  end

  get '/cart' do
    haml :cart
  end

  def add_to_cart(item, quantity)
    session[:cart] << {item: item, quantity: quantity}
  end

  get '/checkout', logged_in: true do
    if session[:cart] == nil || session[:cart] == []
      flash[:warn] = "Your cart is empty"
      redirect back
    end

    # Create new instance of connection to use
    # for transaction
    connection = client
    begin
      connection.query("BEGIN")
      statement = connection.prepare "insert into orders
 (customer_id, restaurant_id, courier_id) values (? , ? , 1)"
      statement.execute session[:user], session[:cart][0][:item][:restaurant_id]
      id = connection.last_id
      statement = connection.prepare "call add_order_item(?,?,?)"

      session[:cart].each do |cart_item|
        statement.execute id, cart_item[:item][:item_id], cart_item[:quantity]
      end
      
      connection.query("COMMIT")

      session[:cart] = []
      flash[:success] = "Order placed!"
      redirect back
    rescue
      connection.query("ROLLBACK")
      flash[:error] = "Something went wrong"
      redirect back
    end
    
  end
 
  post '/cart' do
    if params[:remove_id] != nil
      if session[:cart] != nil
        for cart_item in session[:cart] do
          if (cart_item[:item][:item_id] == params[:remove_id])
            session[:cart].delete cart_item
            flash[:success] = "Item removed successfully"
            redirect back
          end
        end
      end
      flash[:warn] = "Something went wrong"
      redirect back
    elsif params[:update_quantity_id] != nil
      if session[:cart] != nil
        for cart_item in session[:cart] do
          if (cart_item[:item][:item_id] == params[:update_quantity_id])
            cart_item[:quantity] = params[:updated_quantity]
            flash[:success] = "Item updated successfully"
            redirect back
          end
        end
      end
      flash[:warn] = "Something went wrong"
      redirect back
    else
 
    
      item = client_safe_query "select * from item natural join restaurant where item_id = ?", params[:item_id]

      # Attempt to add the item if it exists
      if (item != nil)
        item = item.first
        if (session[:cart] == nil || session[:cart] == [])
          session[:cart] = []
          add_to_cart(item, params[:quantity])
          flash[:success] = "Added item to cart"
        else
          if item[:restaurant_id] != session[:cart][0][:item][:restaurant_id]
            flash[:warn] = "Changed cart for a new restaurant"
            session[:cart] = []
          else
            flash[:success] = "Added item to cart"
          end
          add_to_cart(item, params[:quantity])
        end
      else
        flash[:error] = "Failed to add item to cart"
      end
      
      redirect back
    end
  end
  
  get '/admin', admin: true do
    @current_orders = client.query("select * from current_orders O natural join restaurant join customer C on C.customer_id=O.customer_id join courier U on U.courier_id=O.courier_id")
    puts "CURRENT_ORDERS = #{@current_orders}"
    haml :admin
  end
 
end
