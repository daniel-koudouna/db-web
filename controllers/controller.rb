require 'sinatra/base'
require 'sinatra/flash'

class ApplicationController < Sinatra::Base
  enable :sessions

  register Sinatra::Flash
  
  helpers DatabaseHelper, AssetHelper
  
  set :root, File.expand_path("../../", __FILE__)
  set :views, File.expand_path("../../views",__FILE__)
  set :public_folder, File.expand_path("../../public",__FILE__)

  set(:logged_in) do |r|
    condition do
      if (session[:user] == nil)
        puts "user not logged in!"
        redirect "/cs3101/login" , 303
      end
      return false if (session[:user] == nil)
      u = client_safe_query "select * from customer where customer_id=?", session[:user]
      @user = u.first
      return (@user != nil)
    end
  end

  set(:admin) do |a|
    condition do
      redirect "/cs3101/login" unless (session[:user] != nil)
      u = client_safe_query "select * from customer where customer_id=?", session[:user]
      @user = u.first
      name = @user[:first_name]

      return (name.downcase[/kasim|david/] != nil)
    end
  end

  before do
    if (session[:user] != nil)
       u = client_safe_query "select * from customer where customer_id=?", session[:user]
      @user = u.first
    end
  end
  
end
