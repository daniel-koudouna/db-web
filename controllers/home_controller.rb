class HomeController < ApplicationController

  get '/' do
    params[:name] ||= "%"
    @restaurants = client_safe_query "select * from q4 natural join restaurant where restaurant_name like ? or most_popular_dish like ? limit 3", "%#{params[:name]}%" , "%#{params[:name]}%"
    haml :home
  end

  get '/types' do
    @types = client.query "select distinct type_id from restaurant"
    @cities = client.query "select distinct addr_city from restaurant"
    haml :types
  end

  get '/restaurants/at/:city' do |city|
    @restaurants = client_safe_query "select * from restaurant natural join q4 where addr_city like ?", city
    @types = client.query "select distinct type_id from restaurant"
    haml :restaurants
  end
  
  get '/restaurants/type/:type_id' do |type_id|
    @restaurants = client_safe_query "select * from restaurant natural join q4 where type_id=?", type_id
    @cities = client.query "select distinct addr_city from restaurant"
    haml :restaurants
  end

  get '/restaurants' do
    @restaurants = client.query "select * from restaurant natural join q4"
    @types = client.query "select distinct type_id from restaurant"
    @cities = client.query "select distinct addr_city from restaurant"
    haml :restaurants
  end
  
  get '/restaurants/:restaurant_id' do |restaurant_id|
    res = client_safe_query "select * from restaurant where restaurant_id=?" , restaurant_id
    @restaurant = res.first
    @dishes = client_safe_query "select * from item natural join dish natural join restaurant where restaurant_id=?" , restaurant_id
    @drinks = client_safe_query "select * from item natural join drink natural join restaurant where restaurant_id=?" , restaurant_id
    haml :restaurant
  end

  get '/food' do
    params[:name] ||= "%"
    @dishes = client_safe_query "select * from item natural join dish natural join restaurant where name like ?" , "%#{params[:name]}%"
    @drinks = client_safe_query "select * from item natural join drink natural join restaurant where name like ?",  "%#{params[:name]}%"
    haml :food
  end
end
