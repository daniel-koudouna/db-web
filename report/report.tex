
% Packages
\documentclass{article}

\usepackage{fancyhdr} % Required for custom headers
\usepackage{lastpage} % Required to determine the last page for the footer
\usepackage{extramarks} % Required for headers and footers
\usepackage[usenames,dvipsnames]{color} % Required for custom colors
\usepackage{graphicx} % Required to insert images
\usepackage{listings} % Required for insertion of code
\usepackage{courier} % Required for the courier font
\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template
\usepackage{float}
\usepackage{minted}
\usepackage{hyperref}

\graphicspath{ {images/} }

% Margins
\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1} % Line spacing

% Set up the header and footer
\pagestyle{fancy}
\lhead{\hmwkAuthorName} % Top left header
\chead{\hmwkClass\ : \hmwkTitle} % Top center head
\rhead{\firstxmark} % Top right header
\lfoot{\lastxmark} % Bottom left footer
\cfoot{} % Bottom center footer
\rfoot{Page\ \thepage\ of\ \protect\pageref{LastPage}} % Bottom right footer
\renewcommand\headrulewidth{0.4pt} % Size of the header rule
\renewcommand\footrulewidth{0.4pt} % Size of the footer rule

\setlength\parindent{0pt} % Removes all indentation from paragraphs

% Code
\lstdefinestyle{code}{
	backgroundcolor=\color{white},   
	commentstyle=\color{DarkOrchid},
	keywordstyle=\color{MidnightBlue},
	numberstyle=\tiny\color{Blue},
	stringstyle=\color{Salmon},
	basicstyle=\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2
}



\lstset{style=code}

% Details
\newcommand{\hmwkTitle}{Practical 2 - Database Interface Design} % Assignment title
\newcommand{\hmwkClass}{CS3101} % Course/class
\newcommand{\hmwkAuthorName}{dzk - 140 000 346} % Your name

% Title
\title{
	\vspace{2in}
	\textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
	\normalsize\vspace{0.1in}\small{\today}
}

\author{\textbf{\hmwkAuthorName}}
\date{}

%----------------------------------------------------------------------------------------

\begin{document}
	\maketitle
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.3\textwidth]{coat_of_arms.png}
	\end{figure}
	
	\newpage
	
	\section{Overview}
	In this practical, I was asked to implement a web interface for my database created in practical 1. My interface was to include basic functionality such as displaying data and searching. I fulfilled all the basic requirements, and then implemented some extensions, such as registration and logging into the system, a more sophisticated cart system, google maps integration and an example of an administrator panel.	

	\section{Runtime}

	I implemented my solution in Ruby, using the Sinatra\cite{sinatra} framework. I chose this framework mainly for educational reasons, and since it is a lighter alternative to Rails, which made me think it was suited for a solution of this scale.
	
	I used rack as a web server, which is currently running on my user host server. This was achieved by reverse routing requests on nginx into a uwsgi server running rack. It can be accessed on \url{https://dzk.host.cs.st-andrews.ac.uk/cs3101}. However, the server can also be ran locally. Using the source code provided, first install the dependencies:
	
	\mint{bash}{bundle install}
		
	The configuration options in config.ru should be changed appropriately if running locally to correctly point to the database and configuration file. The server can then be ran from the directory:

	\mint{bash}{rackup}

	config.ru is the default filename that rack searches for, so it doesn't need to be specified.
	
	\section{Design}
	I designed my code to be as modular as possible, with sinatra's design philosophy in mind. The main server code, contained in \lstinline|config.ru| loads all helpers, which contain code available to all the application, and all controllers, which handle routing and then maps them to the appropriate urls, in this case only on /cs3101. 
	
	\subsection{Database connectivity} \label{sec:db}
	I used a helper function to establish database connectivity. This made it simple to execute queries from anywhere in the server. I initially tried to use a single connection, instead of creating a new connection every time a query was needed, but found that this lead to connectivity issues in the long term, as reconnecting was not functioning as expected. I used the school's provided configuration file to access my credentials, to improve security to some extent.
	
	I also added a function to execute prepared statements, shown below, to improve security by escaping user inputs. The library I used provided similar functionality to the PHP bindings. However, I used ruby's splat operator to pass an arbitrary number of parameters to the function, and execute it safely. The downside of this is that the inputs to the function need to be checked to be equal to the inputs in the prepared statement string.
	\inputminted[linenos=true,firstline=15, lastline=18]{ruby}{../helpers/database_helper.rb}
	
	\subsection{Assets}
	Some assets were helpful to improve the appearance of the front-end. However, I didn't want to introduce blobs in the server to store, for example, the logo of each restaurant. I used another helper to load images from the public folder of the server, depending on the name of the restaurant provided, falling back to a default image if the logo does not exist on the server.
	
	\subsection{Routing} \label{sec:controllers}
	Routing is where most of the server logic occurs, much like a PHP file. To keep my application as modular as possible, I created a generic controller, which contains the configuration for my application, such as sessions and flash messages, and extended it to allow mapping into different parts of the application. An typical example of a route is shown below:
	
	\inputminted[linenos=true, breaklines=true, firstline=3, lastline=7]{ruby}{../controllers/home_controller.rb}
	
	As discussed in section \ref{sec:db}, this route queries the database safely for popular restaurants using a prepared statement. The data is not usually used in the controller, rather passed down to the rendering engine to process and display. No more processing needs to be done on the result, since the sql library uses standard ruby iterator patterns to get the results, and no cursors are being used in the query. Notably, the route matches the part of the application that the controller uses as the base. For example, in this case, even though the route is "/", it matches on "/cs3101/", since "/cs3101" is used as the base of the particular controller. I used this pattern since it makes moving different sections of the application into separate sub-directories very easy.
	
	\subsection{Sessions}
	One feature of sinatra is custom conditions, which allowed me to easily create a log in system, with a condition defined below:
	\inputminted[linenos=true, breaklines = true, firstline=15, lastline = 26]{ruby}{../controllers/controller.rb}
	This allowed me to redirect users to the login page if the condition was not satisfied. Of course, the system is not secure at all, but can be extended to include more robust systems, such as server side secret keys. In this way, the code is completely modular and only needs to be changed in this block. Session variables can be accessed anywhere, and can store any data type which can be serialized, like hashes. This allowed me to store the contents of the cart for the user as hashes, and easily retrieve them and manipulate them. 
	
	\subsection{Rendering}
	To render HTML, I used HAML, which is an alternative to the most used ERB format. HAML allows for more concise and expressive code, and direct ruby code evaluation. I used a templating scheme, which means all code follows a common layout, a snippet of which is shown below:
	\inputminted[breaklines = true, linenos=true, firstline=35, lastline=48]{haml}{../views/layout.haml}
	HAML is sensitive to indentation. Any indented components are nested within the parent. HTML divs are so common they are the default tag type. Periods and hashes have their respective HTML meanings. The equal sign allows ruby code execution, and the yield keyword injects the code of the specific page into that position in the layout. An example of a page is shown below:
	\inputminted[breaklines = true, linenos=true]{haml}{../views/restaurants.haml}
	Any html tag can be represented with the \% sign, and ruby variables given in the controllers (section \ref{sec:controllers}) conventionally begin with @. Further HAML code can be inject with the haml helper function.

	\section{Modifications}
	I didn't do any modifications to the schema of my database, but changed names and addresses to reflect real restaurants, in order to use the google maps API.

	\section{Functionality}
	
	\subsection{Searching}
	The user can search the home page for popular restaurants and dishes. Any restaurants or dishes matching the query are displayed, as shown in figure \ref{img:home}.
	
	The user can see all the restaurants in the area, and filter by city or by type. Only cities and types which exist in the database are shown. See figure \ref{img:restaurants}.
	
	Once a restaurant has been selected, the user can view all the dishes and drinks that the restaurant serves, and add them to their cart, shown in figure \ref{img:restaurant}.
	
	Finally, the user can search for all the food served by all restaurants. This is shown in figure \ref{img:food}. The user can also add them to their cart using that page.
	
	\subsection{Google Maps}
	The user can see the location of all restaurants displayed on a map using the Google Maps API. \cite{googlemaps}. This is seen in all figures containing restaurants, such as figure \ref{img:home}.

	\subsection{Registration}
	The user can login and register in the system, shown in figures \ref{img:login} and \ref{img:register}.
	
	The user can register in the system using simple details. An example is shown in \ref{img:register}, and the effects on the database shown in figure \ref{img:register_effect}.	
	
	Once logged in, the user can view their past and present orders placed on the website, shown in figure \ref{img:orders}.
	
	Users "Kasim" and "David", with their respective St. Andrews emails are already registered in the system.
	
	\subsection{Admin Panel}
	If the user's name contains "Kasim" or "David", they are considered an administrator. Administrators can view the admin panel, which shows information about current orders. Unfortunately I didn't have much time to implement more administrator features, but, using sinatra's modular system, it would be easy to route more admin specific requests and html rendered to allow more functionality, such as manipulating the database.

	\subsection{Cart}
	Any visitor can add any number items to their cart, from any location which shows items, without being logged in. However, you must log in to place an order. Users can also change the quantity of items in their cart and remove them. An example of a cart is shown in figure \ref{img:cart}, and the modifications to the database in figure \ref{img:cart_effect}. 
	
	Notably, the checkout operation takes a number of queries to execute, which are carried out as a single transaction from the ruby server, ensuring that no invalid or incomplete orders are placed in the database.
	
	In addition, the cart can only contain items from a single restaurant. An attempt to place an item from a different restaurant will clear the cart and place the new item.
	
	\section{Conclusion}
	In conclusion, I think I have fully completed the requirements of the specification, giving a fully functional interface to my database. I used the best practices to highlight the strengths of my chosen library, and ran it successfully through the school's web server. I explained some of my design decisions to increase readability of my codebase. Finally, I implemented a number of extension to showcase more features such as more complex manipulation of the database, with registrations and orders.
	
	\section{Appendix}
	
	\subsection{Functionality Screenshots}
	
	\begin{figure}[h] 
		\centering
		\includegraphics[width=1\linewidth]{popular.png}
		\caption{The home page}
		\label{img:home}
	\end{figure}
	
	\begin{figure}[h] 
		\centering
		\includegraphics[width=1\linewidth]{restaurants.png}
		\caption{The restaurants page}
		\label{img:restaurants}
	\end{figure}
	
	\begin{figure}[h] 
		\centering
		\includegraphics[width=1\linewidth]{menu.png}
		\caption{The menu page}
		\label{img:restaurant}
	\end{figure}
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=1\linewidth]{search.png}
		\caption{The food page, with a user search of "C"}
		\label{img:food}
	\end{figure}


	\begin{figure}[h]
		\centering
		\includegraphics[width=1\linewidth]{login.png}
		\caption{The login page}
		\label{img:login}
	\end{figure}



	\begin{figure}[h]
		\centering
		\includegraphics[width=1\linewidth]{register.png}
		\caption{The register page}
		\label{img:register}
	\end{figure}

	
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=1\linewidth]{register_res.png}
		\caption{The result of a user registering}
		\label{img:register_effect}
	\end{figure}
	
	
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=1\linewidth]{current_orders_ex.png}
		\caption{The orders page}
		\label{img:orders}
	\end{figure}



	\begin{figure}[h]
		\centering
		\includegraphics[width=1\linewidth]{cart_ex.png}
		\caption{The cart page}
		\label{img:cart}
	\end{figure}
	
	
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=1\linewidth]{order_ex_3.png}
		\caption{The checkout effect on the database}
		\label{img:cart_effect}
	\end{figure}
	
	\begin{thebibliography}{9}
		\bibitem{googlemaps} 
		Goolge Maps APIs
		\\\texttt{https://developers.google.com/maps/}

		\bibitem{sinatra} 
		Sinatra: Classy web-development dressed in a DSL,
		\\\texttt{https://github.com/sinatra/sinatra}
	\end{thebibliography}

	
\end{document}


