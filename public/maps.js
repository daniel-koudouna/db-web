$(".glass-surface").each(function(i) {

    $(this).on("click", function(ev) {
	$(this).find(".glass").css("pointer-events", "all");
    });
});

function initGoogleMaps() {
    var geocoder = new google.maps.Geocoder();
    
    $(".restaurant-map").each( function(index) {
	var id = $(this).attr("id");
	geocoder.geocode( {'address' : id}, function(result, status) {
	    if (status == 'OK') {
		console.log(result);
		var opts = {
		    zoom: 18
		};
		var map = new google.maps.Map(document.getElementById(id), opts);
		map.setCenter(result[0].geometry.location);
		var marker = new google.maps.Marker({
		    map: map,
		    position: result[0].geometry.location
		});
	    }
	});
    });
}
