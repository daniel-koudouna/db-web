if (ENV['HOME'] == nil)
	ENV['HOME'] = '/cs/home/dzk'
	ENV['GEM_HOME'] = "#{ENV['HOME']}/.gem/ruby"
	ENV['GEM_PATH'] = "#{ENV['GEM_HOME']}"
	
	require 'rubygems'
	Gem.clear_paths
end

require 'sinatra/base'

Dir.glob("./helpers/*.rb").each &method(:require)

# Load all the controllers which handle the routing
Dir.glob("./controllers/*.rb").each &method(:require)


# Map the controllers to the specific namespaces
map('/cs3101') do
  use UserController
  run HomeController
end

