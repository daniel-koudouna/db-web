require 'mysql2'

module DatabaseHelper
  Mysql2::Client.default_query_options.merge!(symbolize_keys: true)

  def client
    Mysql2::Client.new({
                        host: "localhost",
                        default_file: "/var/cs/mysql/dzk/my.cnf",
                        database: "dzk_cs3101_db",
                        reconnect: true
                       })
  end

  def client_safe_query(query, *args)
    statement = client.prepare query
    statement.execute(*args)
  end
end
