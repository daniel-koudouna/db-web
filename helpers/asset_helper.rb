module AssetHelper

  IMAGE_FOLDER = "images/"
  RESTAURANT_FOLDER = "images/restaurants/"
  
  def image_for(restaurant)

    name = restaurant[:restaurant_name].downcase.gsub " " , "_"
    png_name = RESTAURANT_FOLDER + name + ".png"
    jpg_name = RESTAURANT_FOLDER + name + ".jpg"
    type_png_name = RESTAURANT_FOLDER + restaurant[:type_id].downcase + ".png"
    type_jpg_name = RESTAURANT_FOLDER + restaurant[:type_id].downcase + ".jpg"

    default = RESTAURANT_FOLDER + "default.jpg"

    for path in [png_name, jpg_name, type_png_name, type_jpg_name] do
      
      if (File.exist?("public/" + path ))
        return url(path)
      end
    end

    url(default)
  end
end
